import React, { useState, useEffect } from "react";
import Form from "./components/Form.jsx";
import ToDoList from "./components/ToDoList.jsx";
import "./App.css";

export default function App() {

  const [inputText, setInputText] = useState("");
  const [todos, setToDos] = useState([]);
  const [status, setStatus] = useState("all");
  const [filteredTodos, setFilteredTodos] = useState([]);


  useEffect(() => {
    getLocalTodos();
  }, [])

  useEffect(() => {
    filterHandler();
    saveLocalTodos();
  }, [todos, status]);

  //Empty array === run only once after function is called with useEffect

  const filterHandler = () => {
    switch (status) {
      case "completed":
        setFilteredTodos(todos.filter((todo) => todo.completed === true));
        break;
      case "uncompleted":
        setFilteredTodos(todos.filter((todo) => todo.completed === false));
        break;
      default:
        setFilteredTodos(todos);
        break;
    }
  };

  const saveLocalTodos = () => {
      localStorage.setItem('todos', JSON.stringify(todos))
    }

  const getLocalTodos = () => {
     if(localStorage.getItem('todos') === null) {
      localStorage.setItem('todos', JSON.stringify([]));
    } else {
      let toDoLocal = JSON.parse(localStorage.getItem('todos'))
      setToDos(toDoLocal);
    }
  };

  return (
    <div className="App">
      <header>
        <h1>Tomislav's To Do List</h1>
      </header>
      <Form
        inputText={inputText}
        todos={todos}
        setToDos={setToDos}
        setInputText={setInputText}
        setStatus={setStatus}
      />
      <ToDoList setToDos={setToDos} todos={todos} filteredTodos={filteredTodos} />
    </div>
  );
}
