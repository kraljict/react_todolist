import React from 'react';

const Form = ({setInputText,inputText, todos, setToDos, setStatus}) => {
    const inputTextHandler = (e) => {
        setInputText(e.target.value);
    };

const submitToDoHandler = (e) => {
    e.preventDefault();
    setToDos([
        ...todos, {text: inputText, completed: false, id: Math.random() * 100 },
    ])

    setInputText("");

}

const statusHandler = (e) => {
    console.log(e.target.value)
    setStatus(e.target.value)
}

    return (
        <form>
            <input value={inputText} onChange={inputTextHandler} type="text" className="todo-input" />
            <button onClick={submitToDoHandler} className="todo-button" type="submit">
                <i className="fas fa-plus-square"></i>
            </button>

            <div className="select">
                <select onChange={statusHandler} name="todos" className="filter-todo">
                    <option value="all">All</option>
                    <option value="uncompleted">Incomplete</option>
                    <option value="completed">Complete</option>
                </select>
            </div>
        </form>
    )
}


export default Form;